# Created 2016-08-30 Tue 08:17
#+TITLE: McCLIM development reports
#+AUTHOR: Daniel Kochmański
* Tasks
** Refresh McCLIM web resources
- [X] deadlinks
- [X] blog, rss
- [X] wiki, screenshots
- [X] mailing list
- [X] cliki.net entry update
- [X] add the rss feed to the planet.lisp.org
- [X] write tutorial how to manage mcclim website
- [ ] webcast of creating application

** Create the internals documentation
- [X] document external dependencies
- [X] document internal modules and dependencies
- [X] identify parts which may be plugged with the portability layers

** Development
- [X] verify McCLIM portability (implementations on which it works)
- [X] clean the code and asdf systems hierarchy
  - [X] don't keep code in a top-level (except mcclim.asd)
  - [X] add systems for Experimental/ and Extensions/
  - [X] add system for the debugger
- [X] Prettify
  - [X] make truetype the default font for mcclim
  - [X] use a pixielook by default (?)
  X11 core protocol should be mcclim-xcore
- [-] Plug baroque modules with the portability layers
  (gray-streams, mop, opticl for images)
  - [X] streams        :: trivial-gray-streams
  - [ ] clim-mop       :: closer-mop
  - [-] mcclim-bitmaps :: opticl
    - [X] use opticl
    - [ ] create an image test-grid with various format/variant
      samples to verify, which do we handle correctly
    - [ ] implement rotations etc. (spectacle does that)
  - [X] clim-ffi       :: /removed/
  - [ ] utils          :: uiop, quickutils, alexandria (?)
- [ ] features
  - [ ] editor-output-history

** Notes
- Consider how the testing should be done
- Don't work on CLX itself unless inevitable

** Bounties
[[https://www.bountysource.com/issues/35049403-clx-input-english-layout]]
[[https://www.bountysource.com/issues/31529257-bug-setf-frame-command-table]]
[[https://www.bountysource.com/issues/36946716-loading-device-fonts-via-clx]]

** Expenses
| What?              | When?            | $$$ |
|--------------------+------------------+-----|
| bounty (issue #35) | [2016-08-16 Tue] | 100 |
| bounty (issue #21) | [2016-08-16 Tue] | 100 |
| bounty (issue #65) | [2016-08-16 Tue] | 100 |
| Iteration 1 (44h)  | [2016-08-29 Mon] | 600 |
| Iteration 2 (56h)  | [2016-10-01 Mon] | 600 |
| Iteration 3 (43h)  | [2016-10-31 Mon] | 500 |
| Iteration 4 (52h)  | [2016-12-02 Fri] | 600 |
| Iteration 5 (42h)  | [2016-12-28 Wed] | 600 |


* DONE Iteration 0 (150 h)
** summary

The first iteration was resolving around identifying problems and
cleaning up the code base. We've moved sources to their corresponding
directories, cleaned up system definition file, fixed dependency tree,
replaced big parts of code with a libraries like trivial-gray-stream
and opticl. Removed defunct backends (all except clx, beagle and
null).

We've also refreshed the website, mailing list and created the IRC
channel #clim. Some information on the social news (planet.lisp.org,
reddit.com, twitter.com) spawned interest in McCLIM and ensured us,
that there are people who are willing to use it and that it's a
worthwhile effort.

** Work reports (67h)
*** [2016-05-21 Sat] [6h] (6h)
 - gaining access to mcclim resources
 - adding coleslaw to the common-lisp.net
 - creating McCLIM theme for coleslaw
 - fixing broken links, adding resources
 - make the mcclim.png logo a vector graphic
 - add scripts and refresh the mcclim-website

*** [2016-05-23 Mon] [6h] (12h)
 - add responsiveness to the website
 - submit rss feed to planet.lisp.org
 - spread the information about active McCLIM development (HN, reddit)
 - moderate the cliki.net entry
 - generating the system dependency graph
 - merging bitmap backends into one system
 - cherry-pick commits from Gabriel Laddel

*** [2016-05-24 Tue] [6h] (18h)
 - finishing the dependency graph builder
 - working on internals in par with the documentation
 - lurking through the Apps/, fixing .asd's
 - documenting dependences and finding things which needs replacement

*** [2016-05-25 Wed] [9h] (27h)
 - checking the portability
 - improving the internals notes
 - moving files and directories (asd systems etc)

*** [2016-05-26 Thu] [6h] (33h)
 - further cleaning
 - moderating text files
 - improving the internal.org report
 - moving core systems to Core/
 - moving substrated to Libraries/
 - extracting tab-layout as a separate system
 - removing backend OpenGL and graphic-forms
 - removing clim-ffi package

*** [2016-05-27 Fri] [2h] (35h)
 - extracting pixie look as a separate system
 - renaming mcclim-truetype to clim-fonts/truetype, fixing it's
   dependencies
 - providing clim-clx/pretty system which clears the dependency tree
 - manual testing performance (truetype is slow)

*** [2016-05-28 Sat] [4h] (39h)
 - website, screenshots
*** [2016-05-31 Tue] [8h] (47h)
 - systems cleanup
 - testing various implementations, performance, freetype
 - registering irc channel, modifying website
*** [2016-06-02 Thu] [6h] (53h)
 - trivial-gray-streams
 - closer-mop
*** [2016-06-07 Tue] [4h] (57h)
 - opticl, bitmaps
*** [2016-06-08 Wed] [4h] (61h)
 - studying sources
*** [2016-06-09 Thu] [2h] (63h)
 - clim-spec, bitmaps
 - studying the mcclim manual
*** [2016-06-10 Fri] [4h] (67h)
 - studying source code
 - nailing bug with pointer-documentation pane
** Work reports (exact timing – 83h)
    :LOGBOOK:
    CLOCK: [2016-06-13 Mon 15:50]--[2016-06-13 Mon 18:20] =>  2:30
    CLOCK: [2016-06-16 Thu 14:28]--[2016-06-16 Thu 19:08] =>  4:40
    CLOCK: [2016-06-16 Thu 09:00]--[2016-06-16 Thu 12:50] =>  3:50
    CLOCK: [2016-06-20 Mon 10:05]--[2016-06-20 Mon 13:09] =>  3:04
    CLOCK: [2016-06-21 Tue 10:55]--[2016-06-21 Tue 14:00] =>  3:05
    CLOCK: [2016-06-22 Wed 10:04]--[2016-06-22 Wed 14:17] =>  4:13
    CLOCK: [2016-06-23 Thu 21:05]--[2016-06-23 Thu 21:40] =>  0:35
    CLOCK: [2016-06-23 Thu 16:21]--[2016-06-23 Thu 19:49] =>  3:28
    CLOCK: [2016-06-23 Thu 11:00]--[2016-06-23 Thu 13:07] =>  2:07
    CLOCK: [2016-06-24 Fri 17:00]--[2016-06-24 Fri 17:21] =>  0:21
    CLOCK: [2016-06-24 Fri 12:30]--[2016-06-24 Fri 15:55] =>  3:25
    CLOCK: [2016-06-24 Fri 08:30]--[2016-06-24 Fri 12:02] =>  3:32
    CLOCK: [2016-06-28 Tue 08:58]--[2016-06-28 Tue 10:24] =>  1:26
    CLOCK: [2016-07-04 Mon 17:02]--[2016-07-04 Mon 18:15] =>  1:13
    CLOCK: [2016-07-04 Mon 15:00]--[2016-07-04 Mon 16:30] =>  1:30
    CLOCK: [2016-07-05 Tue 08:39]--[2016-07-05 Tue 10:40] =>  2:01
    CLOCK: [2016-07-07 Thu 16:12]--[2016-07-07 Thu 18:12] =>  2:00
    CLOCK: [2016-07-08 Fri 09:00]--[2016-07-08 Fri 10:13] =>  1:13
    CLOCK: [2016-07-11 Mon 15:31]--[2016-07-11 Mon 16:01] =>  0:30
    CLOCK: [2016-07-11 Mon 11:47]--[2016-07-11 Mon 12:47] =>  1:00
    CLOCK: [2016-07-11 Mon 10:27]--[2016-07-11 Mon 11:15] =>  0:48
    CLOCK: [2016-07-11 Mon 08:42]--[2016-07-11 Mon 10:07] =>  1:25
    CLOCK: [2016-07-12 Tue 11:00]--[2016-07-12 Tue 14:00] =>  3:00
    CLOCK: [2016-07-13 Wed 10:31]--[2016-07-13 Wed 14:08] =>  3:37
    CLOCK: [2016-07-29 Fri 08:20]--[2016-07-29 Fri 11:45] =>  3:25
    CLOCK: [2016-07-25 Mon 09:00]--[2016-07-25 Mon 13:00] =>  4:00
    CLOCK: [2016-07-21 Thu 14:00]--[2016-07-21 Thu 18:00] =>  4:00
    CLOCK: [2016-07-21 Thu 08:00]--[2016-07-21 Thu 11:05] =>  3:05
    CLOCK: [2016-07-19 Tue 13:26]--[2016-07-19 Tue 14:52] =>  1:26
    CLOCK: [2016-07-18 Mon 14:54]--[2016-07-18 Mon 17:26] =>  2:32
    CLOCK: [2016-07-15 Fri 11:33]--[2016-07-15 Fri 15:35] =>  4:02
    CLOCK: [2016-07-14 Thu 10:30]--[2016-07-14 Thu 14:30] =>  4:00
    :END:
- [2016-06-13 Mon] ::
  - pointer-documentation bug /further work/
- [2016-06-16 Thu] ::
  - editor-output-history
- [2016-06-20 Mon] ::
  - editor-output-history
- [2016-06-21 Tue] ::
  - studying the spec
  - ovierview, conventions, geometry substrate
- [2016-06-22 Wed] ::
  - Windowing substrate
  - First jab on mirrored sheets issue
- [2016-06-23 Thu] ::
  - mirrored sheets
- [2016-06-24 Fri] ::
  - mirrored sheets (major progress)
- [2016-06-28 Tue] ::
- [2016-07-04 Mon] ::
  - mirroring
  - peer review
- [2016-07-05 Tue] ::
  - mirroring
  - output-records
- [2016-07-07 Thu] ::
  - testing de-mirrorized clx backend
  - cleaning the code
- [2016-07-08 Fri] ::
- [2016-07-11 Mon] ::
  - mirroring
  - output-recording
- [2016-07-12 Tue] ::
  - output-recording
- [2016-07-13 Wed] ::

* DONE Iteration 1 (2016-08)
** summary

Dear Supporters,

I'm publishing a progress report for month August with detailed
timelog and brief description of undertakings performed each day. This
file also contains report for the previous iteration sponsored by
Professor Robert Strandh.

[[https://common-lisp.net/project/mcclim/static/documents/status-reports.org]]

The most important achievement was securing funds for a few months of
work with the =Bountysource= crowdfunding campaign. We've created some
bounties to attract new developers. =#clim= channel on =Freenode= is
active and we seem to regain the user base what results in interesting
discussions, knowledge sharing and increased awareness about the
project among non-clim users.

We have also gained valuable feedback about user expectations
regarding the further development and issues which are the most
inconvenient for them. We've added a new section on the website
[[https://common-lisp.net/project/mcclim/involve]] which addresses some
common questions and doubts and we have created a wiki on GitHub (not
very useful yet [[https://github.com/robert-strandh/McCLIM/wiki]]).

During this time we are constantly working on identifying and fixing
issues, cleaning up the code base and thinking about potential
improvements. Curious reader may consult the git repository log, IRC
log and read the logbook.

As a side note, I've exceeded time meant for this iteration by four
hours, but I'm treating it as my free time. Additionally people may
have noticed that I did some works on CLX not specifically related to
McCLIM – this development was done on my own time as well.

Also, to address a few questions regarding our agenda – our roadmap is
listed here: [[https://common-lisp.net/project/mcclim/involve]]. That
means among other things, that we are concentrated on finishing and
polishing the CLX backend and we are currently *not* working on any
other backends.

If you have any questions, doubts or suggestions – please contact me
either with email (daniel@turtleware.eu) or on IRC (my nick is
jackdaniel).

Sincerely yours,
Daniel Kochmański

** logbook
  :LOGBOOK:
  CLOCK: [2016-08-22 Mon 12:15]--[2016-08-22 Mon 14:31] =>  2:16
  CLOCK: [2016-08-22 Mon 08:25]--[2016-08-22 Mon 11:51] =>  3:26
  CLOCK: [2016-08-22 Mon 05:51]--[2016-08-22 Mon 07:58] =>  2:07
  CLOCK: [2016-08-20 Sat 06:47]--[2016-08-20 Sat 09:47] =>  3:00
  CLOCK: [2016-08-19 Fri 18:30]--[2016-08-19 Fri 22:30] =>  4:00
  CLOCK: [2016-08-18 Thu 11:00]--[2016-08-18 Thu 11:34] =>  0:30
  CLOCK: [2016-08-17 Wed 14:30]--[2016-08-17 Wed 16:15] =>  1:45
  CLOCK: [2016-08-17 Wed 11:30]--[2016-08-17 Wed 13:00] =>  1:30
  CLOCK: [2016-08-17 Wed 09:00]--[2016-08-17 Wed 10:00] =>  1:00
  CLOCK: [2016-08-16 Tue 16:30]--[2016-08-16 Tue 19:30] =>  3:00
  CLOCK: [2016-08-16 Tue 13:13]--[2016-08-16 Tue 16:13] =>  3:00
  CLOCK: [2016-08-16 Tue 09:50]--[2016-08-16 Tue 12:50] =>  3:00
  CLOCK: [2016-08-15 Mon 06:18]--[2016-08-15 Mon 11:18] =>  5:00
  CLOCK: [2016-08-12 Fri 14:00]--[2016-08-12 Fri 15:00] =>  1:00
  CLOCK: [2016-08-12 Fri 10:00]--[2016-08-12 Fri 12:00] =>  2:00
  CLOCK: [2016-08-11 Thu 11:01]--[2016-08-11 Thu 13:47] =>  2:46
  CLOCK: [2016-08-02 Tue 15:00]--[2016-08-02 Tue 16:42] =>  1:42
  CLOCK: [2016-08-02 Tue 12:19]--[2016-08-02 Tue 14:06] =>  1:47
  CLOCK: [2016-07-29 Fri 16:13]--[2016-07-29 Fri 17:20] =>  1:07
  :END:
- [2016-07-29 Fri] ::
     - proofreading and moderating tutorial #1
- [2016-08-02 Tue] ::
     - blog post #1
     - xrender font extension
- [2016-08-11 Thu] :: - merging CLX PR's
     - working on McCLIM issues on GitHub
     - tutorial #1
     - fixing bug with the presentation types
- [2016-08-12 Fri] :: - bountysource
     - McCLIM repository maintenance
- [2016-08-15 Mon] :: - creating road map, FAQ and contribution guide
     - website maintenance
     - gathering feedback and creating wiki to store ideas
- [2016-08-16 Tue] :: - first bounties
     - fundraiser summary (blogpost)
     - maintaining the status report
     - "last showstopping error" (issue #55)
- [2016-08-17 Wed] :: - issue #55 (CLX #42 – fixed)
     - managing GitHub (wiki, issues, pr)
- [2016-08-18 Thu] :: - issue #55
- [2016-08-19 Fri] :: - pr #67
     - issues #55, #31, [x] #67, [x] #71
     - pixie look
     - standard-backend
- [2016-08-20 Sat] :: - wiki
     - issues #70
     - manual
     - removing goatee and pixie
     - text-editor-gadget refactor
- [2016-08-22 Mon] :: - reduce make-translator-fun abstraction (and fix bugs/apps)
- [2016-08-28 Sun] :: - remove rgb-image.lisp (code duplication)

* DONE Iteration 2 (2016-09)
** summary
Dear Community,

During this iteration my main focus was targeted towards refactoring
font implementation for McCLIM. Major changes were applied to both the
CLX backend and the TrueType extension - fixing issues and improving
functionality. A brief description of the TrueType extension is added
in the corresponding system =mcclim-fonts/truetype= (in the form of
=README.md= file).

The branch with mentioned changes waits for a peer review and will be
hopefully merged soon. Since it is a vast change any critical feedback
is very welcome:

https://github.com/robert-strandh/McCLIM/pull/97

In the meantime I've worked a bit on [[https://github.com/robert-strandh/McCLIM/issues/55][issue #55]] (without great
success), answered questions on IRC, responded to reported issues and
merged some pull requests. I'm very pleased that we have a modest, yet
constant stream of contributions - many thanks to all contributors.

Posted bounties have not been claimed yet and unfortunately we haven't
found a second developer for the agreed price so far.

A detailed report is available at:

https://common-lisp.net/project/mcclim/static/documents/status-reports.org

If you have any questions, doubts or suggestions – please contact me
either with email (daniel@turtleware.eu) or on IRC (my nick is
=jackdaniel=).

Sincerely yours,
Daniel Kochmański

** logbook
   :LOGBOOK:
   CLOCK: [2016-10-03 Mon 13:00]--[2016-10-03 Mon 14:00] =>  1:00
   CLOCK: [2016-10-03 Mon 09:00]--[2016-10-03 Mon 11:30] =>  2:30
   CLOCK: [2016-10-01 Sat 16:00]--[2016-10-01 Sat 18:30] =>  2:30
   CLOCK: [2016-10-01 Sat 14:30]--[2016-10-01 Sat 15:30] =>  1:00
   CLOCK: [2016-10-01 Sat 10:30]--[2016-10-01 Sat 12:00] =>  1:30
   CLOCK: [2016-10-01 Sat 09:15]--[2016-10-01 Sat 09:45] =>  0:30
   CLOCK: [2016-09-30 Fri 19:20]--[2016-09-30 Fri 20:20] =>  1:00
   CLOCK: [2016-09-30 Fri 17:00]--[2016-09-30 Fri 17:30] =>  0:30
   CLOCK: [2016-09-30 Fri 12:30]--[2016-09-30 Fri 15:15] =>  2:45
   CLOCK: [2016-09-30 Fri 08:30]--[2016-09-30 Fri 11:30] =>  3:00
   CLOCK: [2016-09-29 Thu 14:00]--[2016-09-29 Thu 15:00] =>  1:00
   CLOCK: [2016-09-29 Thu 12:30]--[2016-09-29 Thu 13:00] =>  0:30
   CLOCK: [2016-09-29 Thu 10:00]--[2016-09-29 Thu 11:30] =>  1:30
   CLOCK: [2016-09-29 Thu 10:20]--[2016-09-29 Thu 10:50] =>  0:30
   CLOCK: [2016-09-28 Wed 15:45]--[2016-09-28 Wed 16:45] =>  1:00
   CLOCK: [2016-09-28 Wed 14:00]--[2016-09-28 Wed 15:15] =>  1:15
   CLOCK: [2016-09-28 Wed 08:40]--[2016-09-28 Wed 12:40] =>  4:00
   CLOCK: [2016-09-27 Tue 12:30]--[2016-09-27 Tue 16:00] =>  3:30
   CLOCK: [2016-09-21 Wed 08:45]--[2016-09-21 Wed 10:15] =>  1:30
   CLOCK: [2016-09-19 Mon 10:00]--[2016-09-19 Mon 12:00] =>  2:00
   CLOCK: [2016-09-17 Sat 09:30]--[2016-09-17 Sat 11:00] =>  1:30
   CLOCK: [2016-09-16 Fri 15:00]--[2016-09-16 Fri 17:00] =>  2:00
   CLOCK: [2016-09-16 Fri 11:00]--[2016-09-16 Fri 14:00] =>  3:00
   CLOCK: [2016-09-15 Thu 16:00]--[2016-09-15 Thu 17:30] =>  1:30
   CLOCK: [2016-09-15 Thu 11:30]--[2016-09-15 Thu 14:00] =>  2:30
   CLOCK: [2016-09-12 Mon 12:30]--[2016-09-12 Mon 13:30] =>  1:00
   CLOCK: [2016-09-02 Fri 09:00]--[2016-09-02 Fri 11:00] =>  2:00
   CLOCK: [2016-08-31 Wed 10:00]--[2016-08-31 Wed 11:00] =>  1:00
   CLOCK: [2016-08-30 Tue 14:00]--[2016-08-30 Tue 15:30] =>  1:30
   CLOCK: [2016-08-30 Tue 10:30]--[2016-08-30 Tue 13:30] =>  3:00
   CLOCK: [2016-08-29 Mon 09:00]--[2016-08-29 Mon 13:00] =>  4:00
   :END:
- [2016-08-29 Mon] :: - output-record key parameter [#81 fixed]
  - working on image drawning test grid demo
  - xserver hang [#55]
- [2016-08-30 Tue] :: xserver hang [#55]
- [2016-08-31 Wed] :: xserver hang [#55]
- [2016-09-02 Fri] :: documentation, scraping ideas from IRC
     discussion, issues, xserver hang [#55]
- [2016-09-12 Mon] :: font autodetection [#75]
- [2016-09-15 Thu] :: font autodetection [#75]
- [2016-09-16 Fri] :: xrender-fonts truetype refactoring
- [2016-09-17 Sat] :: - clx: input: english layout [#35] propagate
     partial fix to CLX project
     - mcclim/clx font refactoring
- [2016-09-19 Mon] :: github (pr's and issues)
- [2016-09-21 Wed] :: refactor xrender-fonts
- [2016-09-27 Tue]--[2016-10-03 Mon] :: refactor xrender-fonts
- [2016-10-03 Mon] :: iteration status report
* DONE Iteration 3 (2016-10)
** summary
Dear Community,

During this iteration I was working on a tutorial about how to create
an application from scratch with McCLIM used as a GUI toolkit with a
detailed description of each step. This is targeted at beginners who
want to write their own project with a =CLIM= interface. The tutorial
isn't finished yet, but I expect to publish it soon.

The =font-autoconfigure= branch was successfully merged to the master
branch and various issues were closed thanks to that. One of them is
bounty issue #65. Since I don't know how to cancel the bounty, I'm
claiming it ($100), and I'm withdrawing from the McCLIM account the
usual amount with this $100 subtracted.

I have replaced the =clim-listener= non-portable utilities with the
=osicat= portability layer and the =alexandria= library. Changes are
present in the =develop= branch (not merged yet).

The rest of the time was spent on peer review of the contributions,
merging pull requests, development discussions, questions on IRC and
other maintenance tasks.

A detailed report is available at:

https://common-lisp.net/project/mcclim/static/documents/status-reports.org

If you have any questions, doubts or suggestions – please contact me
either with email (daniel@turtleware.eu) or on IRC (my nick is
=jackdaniel=).

Sincerely yours,
Daniel Kochmański

** logbook
   :LOGBOOK:
   CLOCK: [2016-10-31 Mon 15:30]--[2016-10-31 Mon 16:00] =>  0:30
   CLOCK: [2016-10-31 Mon 10:00]--[2016-10-31 Mon 14:30] =>  4:30
   CLOCK: [2016-10-28 Fri 11:00]--[2016-10-28 Fri 15:00] =>  4:00
   CLOCK: [2016-10-26 Wed 08:00]--[2016-10-26 Wed 16:00] =>  8:00
   CLOCK: [2016-10-25 Tue 08:30]--[2016-10-25 Tue 13:30] =>  5:00
   CLOCK: [2016-10-24 Mon 09:00]--[2016-10-24 Mon 11:00] =>  2:00
   CLOCK: [2016-10-22 Sat 17:30]--[2016-10-22 Sat 18:00] =>  0:30
   CLOCK: [2016-10-22 Sat 13:30]--[2016-10-22 Sat 15:00] =>  1:30
   CLOCK: [2016-10-22 Sat 10:00]--[2016-10-22 Sat 12:00] =>  2:00
   CLOCK: [2016-10-21 Fri 08:00]--[2016-10-21 Fri 18:00] =>  10:00
   CLOCK: [2016-10-11 Tue 18:40]--[2016-10-11 Tue 20:10] =>  1:30
   CLOCK: [2016-10-11 Tue 16:30]--[2016-10-11 Tue 18:00] =>  1:30
   CLOCK: [2016-10-11 Tue 10:30]--[2016-10-11 Tue 12:30] =>  2:00
   :END:
- plug util.lisp in clim-listener with =osicat=
- writing tutorial
- maintenance
- merging font-auto branch
- issues: #65
- irc (design discussions, questions from users)
* DONE Iteration 4 (2016-11)
** summary
Dear Community,

During this iteration I have continued to work on the tutorial,
improving documentation, working on issues and assuring CLIM II
specification compatibility.

Most notable change is that argument type in =define-command= is not
evaluated (but if it is quoted it still works for backward
compatibility reasons). I've also started some refactoring of the
=frames= module implementation.

The tutorial work takes some time because I try to fix bugs when I
encounter them to make the walkthrough as flawless as possible. While
I'm not overly satisfied with the tutorial writing progress and its
current shape, this work results in benefit of improving the code and
the documentation.

The documentation chapter named "Demos and applications" has been
updated to reflect the current state of the code base. Some additional
clarifications about the pane order and pane names have been added
added to it. I've updated the website to include the external
tutorials and include the =Guided Tour= in the =Resources=
section. The manual has been updated as well.

The rest of the time was spent on peer review of the contributions,
merging pull requests, development discussions, questions on IRC and
other maintenance tasks.

Alessandro Serra has created a =Raster Image Backend= – a backend
similar to =PostScript=, but having PNG as its output. See "Drawing
Tests" in =clim-examples= and the chapter "Raster Image backend" in
the Manual.

A detailed report is available at:

https://common-lisp.net/project/mcclim/static/documents/status-reports.org

If you have any questions, doubts or suggestions – please contact me
either by email (daniel@turtleware.eu) or on IRC (my nick is
=jackdaniel=).

Sincerely yours,
Daniel Kochmański

** logbook
   :LOGBOOK:
   CLOCK: [2016-12-02 Fri 09:30]--[2016-12-02 Fri 12:00] =>  2:30
   CLOCK: [2016-12-01 Thu 13:30]--[2016-12-01 Thu 14:30] =>  1:00
   CLOCK: [2016-12-01 Thu 09:00]--[2016-12-01 Thu 10:30] =>  1:30
   CLOCK: [2016-11-30 Wed 09:00]--[2016-11-30 Wed 17:15] =>  8:15
   CLOCK: [2016-11-28 Mon 08:00]--[2016-11-28 Mon 15:15] =>  7:15
   CLOCK: [2016-11-26 Sat 18:00]--[2016-11-26 Sat 20:00] =>  2:00
   CLOCK: [2016-11-25 Fri 10:00]--[2016-11-25 Fri 12:00] =>  2:00
   CLOCK: [2016-11-24 Thu 15:00]--[2016-11-24 Thu 17:00] =>  2:00
   CLOCK: [2016-11-24 Thu 10:00]--[2016-11-24 Thu 14:00] =>  4:00
   CLOCK: [2016-11-23 Wed 14:00]--[2016-11-23 Wed 15:00] =>  1:00
   CLOCK: [2016-11-22 Tue 11:00]--[2016-11-22 Tue 13:00] =>  2:00
   CLOCK: [2016-11-17 Thu 11:15]--[2016-11-17 Thu 14:15] =>  3:00
   CLOCK: [2016-11-16 Wed 19:30]--[2016-11-16 Wed 21:00] =>  1:30
   CLOCK: [2016-11-16 Wed 10:00]--[2016-11-16 Wed 14:30] =>  4:30
   CLOCK: [2016-11-15 Tue 17:00]--[2016-11-15 Tue 18:30] =>  1:30
   CLOCK: [2016-11-15 Tue 08:30]--[2016-11-15 Tue 09:00] =>  0:30
   CLOCK: [2016-11-11 Fri 09:30]--[2016-11-11 Fri 15:30] =>  6:00
   :END:
- Issue #111 (:keystroke argument to commands)
- Improving documentation
  chap-demos-applications – reviewed, updated
  chap-output-protocol – unify documentation
  frame-standard-output interpretation
- Tutorial
- Issue #113
- Issue #114
- Compatibility with CLIM II :: Don't evaluate argument type in
     =define-command=. To preserve backward compatibility treat is as
     always if it is already quoted.
- Issue #117 (fixed)
- Pane reflow on resize dilemma
- frames.lisp refactoring
- Website management
* DONE Iteration 5 (2016-12)
** summary
Dear Community,

During this iteration I have continued working on the tutorial, fixing
the issues and assuring CLIM II specification compatibility.

The most notable change is =standard-application-frame= implementation
refactor which simplifies the code and moves some computation to
creation time (until now we have searched through sheet hierarchy at
each iteration of command loop and at various other occasions at
runtime).

The rest of the time was spent on peer review of the contributions,
merging pull requests, development discussions, questions on IRC and
other maintenance tasks.

Alessandro Serra has created a =Framebuffer Backend= – a working proof
of concept that McCLIM may work on top of a frame buffer. Albeit a bit
slow, you may find instructions how to run it here:

https://github.com/robert-strandh/McCLIM/wiki/CLX-backend(s)

A detailed report is available at:

https://common-lisp.net/project/mcclim/static/documents/status-reports.org

If you have any questions, doubts or suggestions – please contact me
either by email (daniel@turtleware.eu) or on IRC (my nick is
=jackdaniel=).

Happy new year!

Sincerely yours,
Daniel Kochmański

** logbook
   :LOGBOOK:
   CLOCK: [2016-12-28 Wed 14:00]--[2016-12-28 Wed 15:45] =>  1:45
   CLOCK: [2016-12-28 Wed 09:00]--[2016-12-28 Wed 13:30] =>  4:30
   CLOCK: [2016-12-27 Tue 16:15]--[2016-12-27 Tue 18:00] =>  1:45
   CLOCK: [2016-12-27 Tue 11:00]--[2016-12-27 Tue 16:00] =>  5:00
   CLOCK: [2016-12-23 Fri 10:00]--[2016-12-23 Fri 20:30] => 10:30
   CLOCK: [2016-12-22 Thu 16:00]--[2016-12-22 Thu 20:00] =>  4:00
   CLOCK: [2016-12-17 Sat 09:30]--[2016-12-17 Sat 13:00] =>  3:30
   CLOCK: [2016-12-16 Fri 08:00]--[2016-12-16 Fri 12:00] =>  4:00
   CLOCK: [2016-12-15 Thu 14:00]--[2016-12-15 Thu 17:00] =>  3:00
   CLOCK: [2016-12-14 Wed 08:00]--[2016-12-14 Wed 12:00] =>  4:00
   :END:

- tutorial
- issues #118, #122, #123, #125
- #124 accept default method bug
- simplified-clim extension
- draggable-graph regression
- frames.lisp refactoring
- reviewing framebuffer backend
- issue Q/A
