
## McCLIM website management ##

McCLIM uses the
[sclp](https://gitlab.common-lisp.net/dkochmanski/sclp) as a base for
it's website. This short tutorial will describe the deployment
process. To build a website we use
[coleslaw](https://github.com/kingcons/coleslaw) – a static website
generator.

### Adding content ###

To add a news entry you have to clone the repository and add to the
directory `posts/` the file (in markdown with with an extra header):

```markdown
;;;;;
title: Awesome blog entry
tags: tag1, tag2, my-awesome-tag-3
date: 2016-05-22 12:30
format: md
author: John Doe
;;;;;

The blog entry content follows.
* Point 1
* Point 2
* [Point 3](https://point-3.example.org)

Voila.
```

The steps for creating a static page are almost the same. You need to
put the file in the `pages/` directory and provide slightly different
header which indicates the URL:

```markdown
;;;;;
url: new.html
;;;;;

Page content follows.
```

Additionally, if you want to create this website entry on the
navigation bar, you need to edit the `.coleslawrc` file's section
`:sitenav` (the section is self-explanatory).

If you want to modify existing page or news just edit the file and
commit changes.

### Adding files not suitable for git ###

GIT isn't well suite for hosting big files. To add new files "heavy"
files to the website put them in the `/project/mcclim/files` directory
– on deployment all files in it will be put in the `static/files`
website's relative path.

Smaller files which belong to the repository may should added to the
`static/` directory (any subdirectory will be fine except `files/`,
which is listed in `.gitignore` and reserved for big files on the host
system.

### Deployment ###

After modifying the website commit the change and push it to the
repository. After that login to your common-lisp.net shell account and
run the script:

```shell
/project/mcclim/bin/regen.sh
```

After this step the website should be deployed.

### References ###

* [SCLP repository](https://gitlab.common-lisp.net/dkochmanski/sclp)
* [Coleslaw documentation](https://github.com/kingcons/coleslaw)
